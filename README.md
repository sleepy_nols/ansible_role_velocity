# Ansible Role Velocity

Ansible role to install and configure a [Velocity](https://papermc.io/software/velocity) Minecraft proxy on Debian-based systems.

This Project is **not** officially part of the PaperMC universe.

<a href="https://gitlab.com/sleepy_nols/ansible_role_velocity/-/pipelines/latest">
<img alt="ansible-lint" src="https://gitlab.com/sleepy_nols/ansible_role_velocity/badges/main/pipeline.svg"/>
</a>

<br>

## Dependencies

  None.

## Variables and Defaults

### Velocity config:

For all defaults see `defaults/main.yml`.

Config documentation can be found [here](https://docs.papermc.io/velocity/configuration).


### General config:

```yml
velocity_servers:
  lobby: "127.0.0.1:30066"
  factions: "127.0.0.1:30067"
  minigames: "10.20.30.40:30068"
```
˄ Configure your servers here. Each key represents the server's name and the value, the IP address of the backend server to connect to.

```yml
velocity_servers_try:
  - "lobby"
  - "minigames"
```
^ In what order we should try servers when a player logs in or is kicked from a server.

```yml
velocity_forced_hosts:
  "lobby.example.com":
    - "lobby"
  "factions.example.com":
    - "fractions"
```
^ Bind specific domains to servers.

```yml
velocity_java_executable: "/usr/bin/java"
```
^ Path to java executable.

```yml
velocity_initial_memory: "1G"
velocity_max_memory: "1G"
```
^ Java memory opts.

```yml
velocity_java_options: "-XX:+UseG1GC -XX:G1HeapRegionSize=4M -XX:+UnlockExperimentalVMOptions -XX:+ParallelRefProcEnabled -XX:+AlwaysPreTouch -XX:MaxInlineLevel=15"
```
^ Java opts.

```yml
velocity_manage_console_alias: true
```
^ If ansible should manage the velocity console alias in shell rc's.

```yml
velocity_shells:
  bash: /etc/bash.bashrc
  zsh: /etc/zsh/zshrc
```
^ Dict of shells (their package names) and their respective full rc paths.

## Install

```
git clone git@gitlab.com:sleepy_nols/ansible_role_velocity.git
```
Clone the Repo.

## Example Playbook

```yml
- name: Deploy Velocity Minecraft proxy
  hosts: minecraft_proxies
  roles:
    - ansible_role_velocity
  vars:
    velocity_servers:
      lobby: "127.0.0.1:30066"

    velocity_servers_try:
      - "lobby"

    # plaintext storage in playbooks
    velocity_forwarding_secret: "{{ lookup('ansible.builtin.password', (inventory_dir + '/playbooks/velocity' + inventory_hostname + '/forwarding_secret')) }}"
```

## Contributing

Contributions and bug reports are welcome :).

## License

GPLv3
